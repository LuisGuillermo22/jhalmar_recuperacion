package facci.pm.piloso.loor.recuperacion.Modelo;

public class Estudiantes {

    private String id, nombres, apellidos, parcial_uno, parcial_dos, imagen;

    public Estudiantes() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getParcial_uno() {
        return parcial_uno;
    }

    public void setParcial_uno(String parcial_uno) {
        this.parcial_uno = parcial_uno;
    }

    public String getParcial_dos() {
        return parcial_dos;
    }

    public void setParcial_dos(String parcial_dos) {
        this.parcial_dos = parcial_dos;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}

package facci.pm.piloso.loor.recuperacion.Actividades;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import facci.pm.piloso.loor.recuperacion.R;

public class MainActivity extends AppCompatActivity {

    private EditText id;
    private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        id = (EditText)findViewById(R.id.idenviar);
        button = (Button)findViewById(R.id.BotonEnviar);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DetalleActivity.class);
                intent.putExtra("id", id.getText().toString().trim());
                startActivity(intent);
            }
        });
    }
}
package facci.pm.piloso.loor.recuperacion.Adaptador;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import facci.pm.piloso.loor.recuperacion.Modelo.Estudiantes;
import facci.pm.piloso.loor.recuperacion.R;

public class AdaptadorEstudiantes extends RecyclerView.Adapter<AdaptadorEstudiantes.ViewHolder>{

    ArrayList<Estudiantes> estudiantesArrayList;

    public AdaptadorEstudiantes(ArrayList<Estudiantes> estudiantesArrayList) {
        this.estudiantesArrayList = estudiantesArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.estudiantes, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        Estudiantes estudiantes = estudiantesArrayList.get(i);
        viewHolder.nombres.setText("Nombres: " + estudiantes.getNombres());
        viewHolder.apellidos.setText("Apellidos: " + estudiantes.getApellidos());
        viewHolder.parcial1.setText("Parcial 1: " + estudiantes.getParcial_uno());
        viewHolder.parcial2.setText("Parcial 2: " + estudiantes.getParcial_uno());
        Picasso.get().load(estudiantes.getImagen()).into(viewHolder.foto);

    }

    @Override
    public int getItemCount() {
        return estudiantesArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView nombres, apellidos, parcial1, parcial2;
        ImageView foto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nombres = (TextView)itemView.findViewById(R.id.NombreE);
            apellidos = (TextView)itemView.findViewById(R.id.ApellidosE);
            parcial1 = (TextView)itemView.findViewById(R.id.Parcial1E);
            parcial2 = (TextView)itemView.findViewById(R.id.PArcial2E);
            foto = (ImageView)itemView.findViewById(R.id.ImagenE);
        }
    }
}
